/**
 * SPDX-FileCopyrightText: 2018 Sybren A. Stüvel <sybren@stuvel.eu>
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const $ = require('jquery');
global.$ = global.jQuery = require('jquery');
require('bootstrap');
require('select2');

$.ajaxSetup({
  beforeSend: function(xhr, settings) {
    function getCookie(name) {
      var cookieValue = null;
      if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
          var cookie = jQuery.trim(cookies[i]);
          // Does this cookie string begin with the name we want?
          if (cookie.substring(0, name.length + 1) == (name + '=')) {
            cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
            break;
          }
        }
      }
      return cookieValue;
    }
    if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
      // Only send the token to relative URLs i.e. locally.
      xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
    }
  }
});
// Our BrainTree interface stuff for payment.
// Using it requires https://js.braintreegateway.com/web/dropin/1.13.0/js/dropin.min.js
//
// Note that the container must be a single element with an ID, like
// $('#bt-dropin').blender_braintree_dropin(...)
(function ( $ ) {
  function selected_gateway() {
    return $('input[name=gateway]:checked').val();
  }

  function gather_error_msg(braintree_error) {
    // Initialise storage for all messages in all recursive calls made from here.
    var message_array = [];

    function recurse(error_ob, depth) {
      if (depth > 10) return;  // infinity is too far away.
      if (typeof error_ob != 'object') return '';

      // If this object has a "message" property, use it.
      if (typeof error_ob.message != 'undefined') {
        message_array.push(error_ob.message);
      }

      // Keep recursing into sub-properties until we find no more "messages" keys.
      for (propname in error_ob) {
        if (propname == 'message') continue;  // already have this one.
        if (!braintree_error.hasOwnProperty(propname)) continue;

        recurse(error_ob[propname], depth+1);
      }
    }

    recurse(braintree_error, 0);
    return message_array.join(" ");
  }

  $.fn.blender_braintree_dropin = function(
      submit_button_selector, error_container_selector,
      payment_method_nonce_selector, client_token, dropin_ui_options) {
    let $container = this;
    let $form = this.closest('form');
    let $submit_button = $(submit_button_selector);
    let submit_button_text = $submit_button.html();
    let $bt_errors = $(error_container_selector);

    let default_options = {
      authorization: client_token,
      // This is why the container must be a single element with an ID:
      container: '#' + $container.attr('id'),
      paypal: {
        flow: 'vault'
      },
      preselectVaultedPaymentMethod: false,
    };

    /* Merge default options with caller-supplied options. */
    var dropin_options = default_options;
    if (typeof dropin_ui_options != 'undefined') {
      for (let prop in dropin_ui_options) {
        dropin_options[prop] = dropin_ui_options[prop];
      }
    }

    function enable_payment_btn() {
      $submit_button
        .removeAttr('disabled')
        .addClass('btn-success')
        .html(submit_button_text);
    }
    function disable_payment_btn(btn_text) {
      $submit_button
        .attr('disabled', 'disabled')
        .removeClass('btn-success')
        .html(btn_text);
    }

    disable_payment_btn('Loading payment interface...');

    braintree.dropin.create(dropin_options, function (createErr, instance) {
      if (createErr != null) {
        console.log(createErr);
        var msg = gather_error_msg(createErr);

        $bt_errors.addClass('text-danger bg-danger').text(msg);
        $submit_button.text('Error loading payment interface')
        return;
      }
      enable_payment_btn();

      // Get the DOM object, rather than the JQuery object.
      // Calling dom_form.submit() won't trigger our submit handler,
      // so we can submit without infinite recursion.
      let dom_form = $form[0];
      $form.submit(function (event) {
        if (selected_gateway() != 'braintree') {
          // This submit call is not for us, so just ignore it.
          // TODO(Sybren): change the architecture so that the submit handler
          // is added/removed as needed instead of having this check here.
          return;
        }

        event.preventDefault();
        disable_payment_btn('Processing...');
        $bt_errors.removeClass('text-danger bg-danger').text('');

        instance.requestPaymentMethod(function (err, payload) {
          // console.log('payload', payload);
          if (err) {
            // After an error, the button should be re-enabled to allow a retry.
            enable_payment_btn();
            // TODO(fsiddi): Investigate the API to properly handle the case of a selected, but not clicked
            // PayPal checkout button.
            if (err.message === 'No payment method is available.') {
              err.message += ' If you selected PayPal, click on the Yellow PayPal Checkout button.'
            }
            $bt_errors.addClass('text-danger bg-danger').text(err.message);
            console.log('Error', err);
            return;
          }
          // Add the nonce to the form and submit (input field is created by the CheckoutForm)
          $('#id_payment_method_nonce').val(payload.nonce);
          dom_form.submit();
        });
      });
    });
    return this;
  };

  // Higher level management of the Braintree drop-in UI; maybe at some point
  // this should be merged into the function above.
  $.fn.braintree_gateway_group = function(
      submit_button_selector, error_container_selector,
      payment_method_nonce_selector, client_token, dropin_ui_options) {
    let $container = this;

    function maybe_create_braintree_ui() {
      // Create the Braintree Drop-in UI?
      if (selected_gateway() != 'braintree') {
        // Different gateway was selected.
        $('#bt-dropin').slideUp('slow');
        return;
      }
      if ($('#bt-dropin').length > 0) {
        // Already created.
        $('#bt-dropin').slideDown('fast');
        return;
      }

      let $bt_dropin_div = $('<div>').attr('id', 'bt-dropin').appendTo($container);

      $bt_dropin_div.blender_braintree_dropin(
        submit_button_selector, error_container_selector,
        payment_method_nonce_selector, client_token, dropin_ui_options);
      $bt_dropin_div.show('slow');
    }
    $('#id_gateway').on('change', maybe_create_braintree_ui);
    maybe_create_braintree_ui();

    return this;
  };
}(jQuery));

function set_preferred_currency(currency) {
  $.post('/preferred-currency', {'preferred_currency': currency})
  .fail(function(error) {
    console.log('Error setting preferred currency:', error);
  })
  .done(function() {
    // Show the change in currency by reloading the current window.
    window.location.reload();
  })
}
